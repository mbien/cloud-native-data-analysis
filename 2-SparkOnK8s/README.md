# Spark and Kafka on K8s

## Preparation
* Before you start, create the cluster following the `part1` steps, until 
```bash
cp config ~/.kube/config
```

* You should also copy the data of your choice to the bucket (preferably in the same region as your cluster)

## Operators
1) Create your namespace of choice, I'll name it `bigdata` here.
```bash
kubectl create ns bigdata
```

2) Install Strimzi Kafka Operator. To do so, run this one-liner (modify namespace accordingly)
```bash
curl -L https://github.com/strimzi/strimzi-kafka-operator/releases/download/0.17.0/strimzi-cluster-operator-0.17.0.yaml \
  | sed 's/namespace: .*/namespace: bigdata/' \
  | kubectl apply -f - -n bigdata
```

3) Check pods running in `bigdata` and ensure that `strimzi-kafka-operator` started succesfully.

4) Inspect crds with `kubectl get crd`. You can see the kafka resource. What does it mean?

5) Inspect the kafka resource [template file](https://raw.githubusercontent.com/strimzi/strimzi-kafka-operator/0.17.0/examples/kafka/kafka-persistent.yaml). Install it in your namespace using: 
```bash
kubectl apply -f https://raw.githubusercontent.com/strimzi/strimzi-kafka-operator/0.17.0/examples/kafka/kafka-persistent.yaml -n bigdata
```

6) Watch the pods running in your namespace. Why it works? Check the crds population:
```bash
kubectl get kafkas -n bigdata
```

7) Kafka cluster is ready! Let's move to spark!

8) Install Spark Operator using convenient helm chart:
```bash
helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
helm install sparkoperator incubator/sparkoperator --namespace bigdata --set enableWebhook=true --set sparkJobNamespace=bigdata
```